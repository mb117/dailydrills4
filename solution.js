const users = require('./1-users.js');

// Q1 Find all users who are interested in video games.
function getUsersIntoGames(users) {
    let result = Object.keys(users).filter((user) => {
        // converting array of strings to string and making everything the same case
        let interest = users[user]["interests"].join("").toLowerCase();

        return interest.includes("video games");
    });

    return result;
}

// console.log(getUsersIntoGames(users));


// Q2 Find all users staying in Germany.
function usersInGermany(users) {
    let result = Object.keys(users).filter((user) => {
        // making strings the same case
        return users[user]["nationality"].toLowerCase() === "germany";
    });

    return result;
}

// console.log(usersInGermany(users));


/*
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
*/ 

// helper function to decide seniority level
function assignSeniority(designation) {
    designation = designation.toLowerCase();

    if ( designation.includes("senior")) {
        return 0;
    } else if ( designation.includes("developer")) {
        return 1;
    } else if ( designation.includes("intern")){
        return 2;
    }
}

// sorts in descending order of Designation and descending order of age
function sortBySeniority(users) {
    let result = Object.entries(users).sort((a,b) => {
        let scoreA, scoreB;

        scoreA = assignSeniority(a[1]["desgination"]);
        scoreB = assignSeniority(b[1]["desgination"]);

        if ( scoreA === scoreB) {
            // same designation then sort by age
            return b[1]["age"] - a[1]["age"];
        } else {
            return scoreA - scoreB;
        }
    });

    return result;
}
// console.log(sortBySeniority(users));


// Q4 Find all users with masters Degree.
function getUsersWithMasters(users) {
    let result = Object.keys(users).filter((user) => {
        // making strings the same case
        return users[user]["qualification"].toLowerCase() === "masters";
    });

    return result;
}

// console.log(getUsersWithMasters(users));


// {
//     "John": {
//         age: 24,
//         desgination: "Senior Golang Developer",
//         interests: ["Chess, Reading Comics, Playing Video Games"],
//         qualification: "Masters",
//         nationality: "Greenland"
//     },
//     "Ron": {
//         age: 19,
//         desgination: "Intern - Golang",
//         interests: ["Video Games"],
//         qualification: "Bachelor",
//         nationality: "UK"
//     }
// }
// Q5 Group users based on their Programming language mentioned in their designation.
function groupByProgrammingLanguage(users) {
    let result = {};

    result = Object.keys(users).reduce((result, user) => {
        let language = users[user]["desgination"].replaceAll("-","");
        language = language.split(" ");

        if ( language.length === 3) {
            if( language[0] === 'Intern') {
                language = language[2];
            } else {
                language = language[1];
            }
        } else {
            language = language[0];
        }

        if ( result.hasOwnProperty(language) === false) {
            result[language] = [];
        }

        result[language].push(user);

        return result;
    }, {});

    
    return result;
}

console.log(groupByProgrammingLanguage(users));
